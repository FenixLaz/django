[![Pipeline status](https://gitlab.crja72.ru/django_2023/students/155661-wwscxe-47231/badges/main/pipeline.svg)](https://gitlab.crja72.ru/django_2023/students/155661-wwscxe-47231/commits/main)
# Установка и запуск

### Для начала Вам нужно склонировать репозиторий:
```git clone git@gitlab.crja72.ru:django_2023/students/155661-wwscxe-47231.git```

### Создайте свой файл .env:
#### Заполните данные:
```SECRET_KEY=your_secret_key```
```DEBUG=debug on(true or false)```
```ALLOWED_HOSTS= your_allowed_hosts```

### Установить виртуальное окружение:
Создайте виртуальное окружение и активируйте его.
```python -m venv venv```

#### Для Windows:
```venv\Scripts\activate```

#### Для Linux:
```source venv/bin/activate```

### Установить зависимости(!для разработки!):
```pip install -r requirements/dev.txt```

### Установить зависимости(!для выпуска!):
```pip install -r requirements/prod.txt```

### Установить зависимости(!для тестирования!):
```pip install -r requirements/test.txt```
### Запусть проект:
```cd lyceum```
```python manage.py runserver```
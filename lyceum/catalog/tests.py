from django.test import TestCase
from django.urls import reverse


class StaticURLTests(TestCase):
    def test_catalog_url(self):
        response = self.client.get(reverse("item_list"))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.content.decode(), 
            "<body>Список элементов</body>"
        )

    def test_item_detail_url(self):
        response = self.client.get(reverse("item_detail", args=[1]))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.content.decode(),
            "<body>Подробно элемент</body>"
        )

    def test_item_detail_endpoint(self):
        response = self.client.get("/catalog/777/")
        self.assertEqual(response.status_code, 200)

    def test_item_detail_error_multi_str_endpoint(self):
        response = self.client.get("/catalog/777adc/")
        self.assertEqual(response.status_code, 404)

    def test_item_detail_error_str_endpoint(self):
        response = self.client.get("/catalog/aloha/")
        self.assertEqual(response.status_code, 404)

    def test_item_detail_error_symbol_endpoint(self):
        response = self.client.get("/catalog/$/")
        self.assertEqual(response.status_code, 404)

    def test_catalog_re_url(self):
        response = self.client.get(reverse("numb", args=[1]))
        self.assertEqual(response.status_code, 200)

    def test_numb_error_symbol_endpoint(self):
        response = self.client.get("/catalog/re/$/")
        self.assertEqual(response.status_code, 404)

    def test_item_detail_not_good_endpoint(self):
        response = self.client.get("/catalog/re/6.66/")
        self.assertEqual(response.status_code, 404)

    def test_item_detail_null_endpoint(self):
        response = self.client.get("/catalog/re/0/")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content.decode(), "<body>0</body>")

    def test_item_detail_good_endpoint(self):
        response = self.client.get("/catalog/re/6/")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content.decode(), "<body>6</body>")

    def test_catalog_converter_url(self):
        response = self.client.get(reverse("converter", args=[1]))
        self.assertEqual(response.status_code, 200)

    def test_catalog_converter_positive_int_url(self):
        response = self.client.get("/catalog/converter/5/")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content.decode(), "<body>5</body>")

    def test_catalog_converter_zero_url(self):
        response = self.client.get("/catalog/converter/0/")
        self.assertEqual(response.status_code, 404)

    def test_catalog_converter_float_url(self):
        response = self.client.get("/catalog/converter/105.323/")
        self.assertEqual(response.status_code, 404)

    def test_catalog_converter_error_symbol_url(self):
        response = self.client.get("/catalog/converter/&&&/")
        self.assertEqual(response.status_code, 404)

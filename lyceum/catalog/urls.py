from django.urls import path, re_path, register_converter

from . import views


class PositiveIntegerConverter:
    regex = "[1-9][0-9]*"

    def to_python(self, value):
        return int(value)

    def to_url(self, value):
        return str(value)


register_converter(PositiveIntegerConverter, "pos_int")

urlpatterns = [
    path("catalog/", views.item_list, name="item_list"),
    path("catalog/<int:pk>/", views.item_detail, name="item_detail"),
    re_path(r"^catalog/re/(?P<numb>\d+)/$", views.numb, name="numb"),
    path(
        "catalog/converter/<pos_int:number>/",
        views.converter,
        name="converter",
    ),
]

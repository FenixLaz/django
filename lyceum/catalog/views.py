from django.http import HttpResponse


def item_list(request):
    return HttpResponse("<body>Список элементов</body>")


def item_detail(request, pk):
    return HttpResponse("<body>Подробно элемент</body>")


def numb(request, numb):
    return HttpResponse(f"<body>{numb}</body>")


def converter(request, number):
    return HttpResponse(f"<body>{number}</body>")

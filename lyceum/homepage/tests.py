from http import HTTPStatus


from django.test import TestCase


class StaticURLTests(TestCase):
    def test_homepage_endpoint(self):
        response = self.client.get("/")
        self.assertEqual(response.status_code, 200)

    def test_coffee_endpoint(self):
        response = self.client.get("/coffee/")
        self.assertEqual(response.status_code, HTTPStatus.IM_A_TEAPOT)
        self.assertEqual(response.content, "<body>Я чайник</body>".encode())

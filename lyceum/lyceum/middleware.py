import re

from django.conf import settings


class ReverseWordsMiddleware:
    counter = 0

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        if settings.ALLOW_REVERSE:
            self.counter += 1
            if self.counter % 10 == 0:

                def reverse_word(match):
                    return match.group(0)[::-1]

                response.content = re.sub(
                    r"\b[а-яА-ЯёЁ]+\b",
                    reverse_word,
                    response.content.decode("utf-8")
                )
        return response

from django.test import override_settings, TestCase


class StaticURLTests(TestCase):
    @override_settings(ALLOW_REVERSE=False)
    def test_middleware_desable_endpoint(self):
        arr = []
        for i in range(10):
            response = self.client.get("/coffee/")
            if i == 9:
                arr += [
                    response.content.decode()
                    .replace("<body>", "")
                    .replace("</body>", "")
                    == "Я чайник"
                ]
        self.assertEqual(any(arr), True, response.content.decode())

    @override_settings(ALLOW_REVERSE=True)
    def test_middleware_enable_endpoint(self):
        arr = []
        for i in range(10):
            response = self.client.get("/coffee/")
            if i == 9:
                arr += [
                    response.content.decode()
                    .replace("<body>", "")
                    .replace("</body>", "")
                    == "Я кинйач"
                ]
        self.assertEqual(any(arr), True, response.content.decode())
